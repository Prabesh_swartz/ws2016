# Nepal Computer Vision and Machine Learning Winter School 2016



This two day winter school aims to inspire Nepalese students in engineering 
sciences to pursue the field of Computer Vision and Machine Learning.

## Organizers and Speakers
 * [Dr. Abhishek Dutta](http://abhishekdutta.org/) (Research Fellow, University of Oxford, UK)
 * [Dr. Bishesh Khanal](http://bishesh.github.io/) (PhD, INRIA Sophia Antipolis, France)

## When and where
 * on Nov. 14 and 15, 2016
 * at Department of Electronics and Computer Engineering (ICTC Building), Pulchowk Campus, Lalitpur.

## Registration
 * [Application for registration](https://npcvml.org/register)
 * registration fee of NRs. 300

## Supported by
We gratefully acknowledge the support of Department of Electronics and 
Computer Engineering, Pulchowk Campus, Lalitpur.


## Structure and Topics
 * This two day winter school will have short lectures.
 * These lectures are designed to cover a wide range of avenues in Computer 
Vision and Machine Learning and are aimed to make the participating students 
curious and excited about this field.
 * We will also explore some topics in Mathematics (at undergraduate level) 
that are indispensable tools for research and development in Computer Vision 
and Machine Learning.
 * We also plan to have some tutorial session (which involves hands on coding 
exercise to re-enforce some of the concepts).
 * We plan to organize this winter school in a very informal setting so that 
the participating students interact with the speakers.

> Education is the kindling of a flame, not the filling of a vessel. — Socrates

## Who can apply
 * We encourage all students currently enrolled in any Engineering Sciences program to apply.
 * If you are curious about Computer Vision and Machine Learning, you are welcome to apply as well.

## License
Nepal Computer Vision and Machine Learning Winter School 
by Abhishek Dutta and Bishesh Khanal is licensed under 
a Creative Commons Attribution-NonCommercial 4.0 International License.
To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/
