# Administrative tools

## Export registered list to a CSV file
 1. Visit [gmail account](https://myaccount.google.com/) and click [control your content](https://myaccount.google.com/privacy?utm_source=OGB#takeout).
 2. Click [create archive](https://takeout.google.com/settings/takeout)
 3. Click "Select None' and then select "Mail". Click "select label" and select "Registrations 2016" to export email corresponding to user registration.
 4. Click "download" and download the archive from the link received in email.
 5. Use the ```ws2016/admin/mailboxes__jsonify_mbox.py``` script to export the email mbox data to a textfile ```Registrations 2016.json``` in JSON format.
```
$ python mailboxes__jsonify_mbox.py ./data/Registrations\ 2016.mbox ./data/Registrations\ 2016.json
```
 6. The script ```/home/thelinuxmaniac/gitlab/ws2016/admin/json2csv.js``` converts this JSON file into a CSV file
```
$ node json2csv.js > ./data/Registration\ 2016.csv
```
 7. This CSV file can be directly imported into [a Google Sheets](https://docs.google.com/spreadsheets/d/1U6lOV1t8lXs8eaUS7BiRp5qmVjq1-Ks4bRjdbGlxRYY/edit#gid=259287325).

