var fs = require('fs');
var path = require('path');

var REG2016_SUBJECT = 'NPCVML User Registration 2016';

var regdata_fn = path.join(__dirname, './data/Registrations 2016.json');
var regdata_str = fs.readFileSync(regdata_fn, {encoding: 'utf8'});
var regdata = JSON.parse(regdata_str);

var user_count = 0;
for (var i=0; i<regdata.length; ++i) {
//for (var i=0; i<1; ++i) {
    if (regdata[i]['Subject'] == REG2016_SUBJECT) {
	var csvdata = [];
	var user = regdata[i]['parts'][0]['content'];
	var reg_datetime = regdata[i]['Date'].trim().replace(',', '');
	user_count += 1;

	csvdata.push(user_count);
	csvdata.push(reg_datetime);

	userdata = user.split('\n');
	for (var j=0; j<userdata.length; ++j) {
	    if ( userdata[j].includes(':') ) {
		var keyval = userdata[j].split(':');
		if (keyval.length == 2) {
		    var key = keyval[0];
		    var val = keyval[1];
		    //console.log(key + ' = ' + val);
		    //console.log(',' + val);

		    val = val.trim().replace(',', '');
		    csvdata.push(val);
		}
	    }
	}
	console.log(csvdata.join(','));
    }
}


