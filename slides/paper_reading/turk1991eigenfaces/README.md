# Eigenfaces for Recognition

This is an implementation of the face recognition method presented in the 
following paper:
Turk, M., & Pentland, A. (1991). Eigenfaces for recognition. Journal of cognitive neuroscience, 3(1), 71-86 
([doi](https://scholar.google.com/scholar?hl=en&q=eigenfaces+for+recognition&btnG=&as_sdt=1%2C5&as_sdtp=&oq=eigenfaces+for+re), [pdf]()http://www.academia.edu/download/30894770/jcn.pdf)
This code was used during the Paper Reading Session of the [NPCVML](https://npcvml.org) Winter School 2016.

Here is one way to structure your thoughts while reading a research paper:

## What problem is the paper addressing?
Reading the paper's abstract gives a quick answer to this question.
Good abstract are written in a way to provide all the essential information 
without using deep or involved technical description. In this paper, the first
sentence of abstract clearly states the purpose of this paper: to talk about a 
method that can recognize a person's face.

If you do not have sufficient background knowledge, it is possible that you will
get lost as you start reading the second sentence or further. Don't despair -- 
youe are not expected to understand every line of the paper. The aim should be 
to first understand the most basic ? of the paper.

## Is this problem interesting to you or your project?
Are you interested in knowing how to recognize human faces in an image? If yes, 
you can proceed to read it further. Otherwise, you should avoid reading papers 
which try to solve problems that are not interesting to you.

## Have the authors actually solved the problem?
Reading the conclusion section of a paper often reveals if the authors have 
succeeded in solving the problem that they mentioned in the abstract (or 
Introduction) section of their paper.

We now analyse the conclusion section of this paper to find out if the authors 
managed to solve the problem of face recognition. In Pg. 84, the paper states, 
"Although it is not an elegant solution to the general recognition 
problem, the eigenface approach does provide a practical solution that is well
fitted to the problem of face recognition". This shows that in this paper, the 
authors have presented a practical solution to the face recognition problem.


## Is there any limitations to this proposed solution?
This information can also be gleaned from the Conclusion section. In Pg. 84, the
paper states, "\[it\] has been shown to work well in a constrained environment".
Not all readers may be familiar with the term "constrained environment". Going 
through the full paper, to find the exact meaning of this term can be a time 
consuming task. Talking to a colleague with knowledge in this area or doing a 
quick google search may help. 

In this paper, this term refers to a controlled environment in which facial 
photo has been captured. Several aspects of this environment -- like 
illumination, pose, etc -- have been constrained to be in certain permissible 
range.

## Do you want to understand how the proposed solution works?
The technical details of the proposed solution usually requires good 
understanding of some concepts and knowledge of previous work. If the paper you 
are reading is not recent, chances are that knowledge of previous work may not 
be needed. However, you need to understand some concepts in order to understand 
how the proposed solution works.

" ... a typical image of size 256 by 256 becomes a vector of dimension 65,536, 
or equivalently a point in 65,536-dimension space"
 * When I was an undergraduate student at Pulchowk Campus, I read this paper 
and this was the sentence that made me restless. I could not understand how a 
256 by 256 image is equivalent to a point in 65,536 dimension space. 
 * So every morning, I came to the Pulchowk Campus library to find out what 
this sentence actually meant. I knew that this is a concept related to matrices 
and therefore scoured the library to find all books discussing matrices.
 * After a lot of trouble, I understood that the concept of vector spaces 
belongs to the Mathematical topic of Linear Algebra.
 * As I explored educational resources on Linear Algebra, I stumbled upon the 
[Linear Algebra course by Gilber Strang](https://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010/) 
at MIT OpenCourseWare.
 * I kept watching the video lectures sequentially until I understood the 
concept of vector spaces (and Linear Algebra in general)

The moral of this story is that to understand any paper, you have to learn 
few concepts and understand some topics. The best way to gain this 
understanding is to explore.

## Can you implement the proposed solution in your personal computer?
See "Summary of Eigenface Recognition Procedure" in Pg. 76. Many papers do not 
include such a clear explanation of the algorithm presented in their papers.
Fortunately, this paper does have one.

## Implementation in Octave

### Dataset : The ORL Database of Faces
 * We chose the ORL Database of Faces that can be downloaded at http://www.cl.cam.ac.uk/research/dtg/attarchive/facedatabase.html
 * It contains a total of 400 images: 10 different images (varying in time, lighting, expression, etc) of 40 subjects.
 * each image is 92 x 112, 8-bit grey levels

### Octave
 * open source tool useful for quick prototyping of algorithms
 * can be downloaded from https://www.gnu.org/software/octave/
 * language syntax quite similar to Matlab

### Concept
 * we often say that a baby has eyes of her mother, face of father, etc
 * so a human face can be represented by a linear sum of eigen faces
 * each eigen face captures a distinct property of human face

### Code
```octave
% Load images from ORL dataset into matrix L

% Compute covariance matrix

% Compute the eigen faces

```
