% This script implements the Eigenfaces method described in the following paper:
% Turk, M., & Pentland, A. (1991). Eigenfaces for recognition. Journal of cognitive neuroscience, 3(1), 71-86 
%
% Author: Abhishek Dutta <http://abhishekdutta.org>
% Nov. 01, 2016

% Required packages:
% 1. image
%     > pkg install image -forge
%     > pkg load image;
close all; clear; clc;

global TRAIN_FACE_DB_DIR = './orl_faces/training/';
global EIGENFACES_DIR = './images/eigenfaces/';
global ORL_IMG_WIDTH = 92;
global ORL_IMG_HEIGHT = 112;
global ORL_IMG_COUNT = 40*10;
  
% Load images from ORL dataset into matrix L
function [d, face_id_list] = load_orl_dataset(dataset_dir)
  global ORL_IMG_WIDTH;
  global ORL_IMG_HEIGHT;
  global ORL_IMG_COUNT;

  d = zeros(ORL_IMG_WIDTH * ORL_IMG_HEIGHT, ORL_IMG_COUNT);
  face_id_list = cell(ORL_IMG_COUNT);

  img_id = 1;
  dirlist = dir(dataset_dir);  
  for i = 1:numel(dirlist)
    if (dirlist(i).isdir)
      userdir = fullfile(dataset_dir, dirlist(i).name);
      user_img_list = dir(userdir);
      for j = 1:numel(user_img_list)
        if user_img_list(j).isdir == 0
          [img_dir, img_filename, img_ext] = fileparts(user_img_list(j).name);
          
          if strcmp(img_ext, '.pgm')
            user_img_fn = fullfile(dataset_dir, dirlist(i).name, user_img_list(j).name);
            img = im2double(imread(user_img_fn)); % height x width (112 x 92)
            d(:, img_id) = reshape(img, ORL_IMG_HEIGHT * ORL_IMG_WIDTH, 1);
            face_id_list{img_id} = dirlist(i).name;
            
            img_id = img_id + 1;
            
            %d(img_id,:) = img;
            %fprintf('\n\t%s', user_img_fn);
          endif
        endif
      endfor
    endif    
  endfor
endfunction

% d = N^2 x M, where
% M = number of images
% NxN = dimension of each image
fprintf('\nLoading ORL face dataset ...');fflush(stdout);
[d, face_id_list] = load_orl_dataset(TRAIN_FACE_DB_DIR);

% Compute the mean image Ψ
fprintf('\nComputing the mean image ...');fflush(stdout);
psi = sum(d, 2) ./ size(d, 2);
%imshow( reshape(psi, 112, 92) ); % show the mean image
%title('Mean Image Ψ'); 

% A = [Ф1 Ф2 ...] N^2 x M
% where,
% Фi = (i-th facial image) - Ψ
fprintf('\nCentering images around the mean value ...');fflush(stdout);
A = bsxfun(@minus, d, psi);

fprintf('\nComuting eigenvectors and eigenvalues ...');fflush(stdout);
% L = A'A
L = A' * A;

% compute eigen value and eigen vectors
% L = U * S * V'
[~, S, V] = svd(L);

% Select the top K eigenvectors
fprintf('\nSaving eigenfaces ...');fflush(stdout);
K = 20;

U = A * V(:,1:K);
for i=1:size(U,2)
  eig_fn = strcat(EIGENFACES_DIR, 'eig_', num2str(i), '.pgm');
  imwrite( reshape(U(:,i), 112, 92), eig_fn);
endfor

% Compute the projection of training images onto the face space
Ap = A' * U;

% Project test image onto the face space
test_img_fn = './orl_faces/test/s1/10.pgm';
test_img = im2double(imread(test_img_fn));
test_img = reshape(test_img, ORL_IMG_HEIGHT * ORL_IMG_WIDTH, 1);
t_faceid = 's1';
test_img = test_img - psi; % subtract the mean image

test_img_omega = test_img' * U;

% Find the closest matching image in the training set
test_img_del = bsxfun(@minus, Ap, test_img_omega);
test_img_euclidean_dist = norm(test_img_del, 2, 'rows');
[test_min_dist, test_pred_index] = min(test_img_euclidean_dist);
test_pred_faceid = face_id_list(test_pred_index);

plot(test_img_euclidean_dist);

fprintf('\nDone\n');fflush(stdout);