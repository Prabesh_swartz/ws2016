Data downloaded from: https://github.com/opennepal/odp-education

 * https://github.com/opennepal/odp-education/tree/master/Number%20of%20School%20in%20Nepal%20from%20the%20year%202007%20-%202011
 * https://github.com/opennepal/odp-education/tree/master/Number%20of%20Students%20in%20Nepal%20from%20the%20year%202007%20-%202011
 * https://github.com/opennepal/odp-education/tree/master/Number%20of%20Teachers%20in%20Nepal%20from%20the%20year%202007%20-%202012
 * [Public expenditure on education (% of GDP)](https://data.humdata.org/dataset/public_expenditure_on_education_of_gdp)
 * [UNESCO Institute for Statistics](http://data.uis.unesco.org/?queryid=181)
