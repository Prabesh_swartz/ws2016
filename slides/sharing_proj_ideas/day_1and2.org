# Title: Day 1 and Day 2 (Nov. 14/15, 2016)
# Nepal Computer Vison and Machine Learning Winter School 2016

* Automatic License plate recognition (Abhishek Dutta)
** Limitations
*** capturig high speed moving vehicles

* Self learning system (Labin Ojha)
** Limitations
*** availability of data
*** world is chaotic and hence difficult to model
** Benefits
*** build a robot and leave it in the world, comes back as a trained expert
*** 
* Surveillance for schools/airport (Aayam K. Shrestha)

* Aircraft collison avoidance system based on vision (Anila Kansakar)

* Nepali sign language detection (Rabin Banjade)
** final year project
** resources available but limited expertise available

* Predicting the solar power productivity at a place (Rabin Banjade)
** build a map ...
** is it feasibile in Nepal where hydro-electricity resources are abundant

* Telemedicine for plants (Ojash Shrestha)

* Face Recognition System for Dementia Patients (Labin Ojha)
** Dementia patients do not recognize their family member
** build a system that recognizes faces and speaks the name and relationship of the person

* Building a repository of digital data for Nepal (Abhishek Dutta)
** extremely difficult to find data related to Nepal in digital form
** data exists but is in papers (or other analog form)
** existing work : http://data.opennepal.net/

* Wildlife monitoring system (Satyarth Upadhaya)
** monitor the movement of wildlife using cameras 

* Archealogical Skull Excavation Reconstruction (Anish Shrestha)
** reconstruct face appearance from skull excavated at archealogical sites

* Machine Learned Medical Expert (Sadhu Ram Basnet)
** Use existing patient diagonosis data to build models that can predict ailments of future unseen patients
* Attractiveness of a Selfie Photo (Labin Ojha)
* Predict the power discharge rate of NTC towers operating on battery (Anila Kansakar)
* Dressing sense advice (Aayam K. Shrestha)
* Braille like system for describing picture (Anish Shrestha)
* Real book to Braille system translation (Labin Ojha)
* Learning the writing style of authors by feeding large amounts of text (Pooja Khanal)
* Using Computer Vision to describe a scene contents using speech for blinds (Ojash Shrestha)
* Changing lighting system in a resturant based on mood of customers (Labin Ojha)

